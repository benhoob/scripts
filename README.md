# scripts

# HoughTransformPlots.C

* First edit the paths and info above the sentence "NO NEED TO MODIFY ANYTHING BELOW THIS LINE". You can leave them as is if you run from lxplus.
* You can make plots for 1 file (set compareFiles false) or 2 files (set compareFiles true).
* Then execute with: root HoughTransformPlots.C.
* Plots will show up in the plots subdirectory.
